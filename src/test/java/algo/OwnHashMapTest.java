package algo;

import java.util.Random;
import org.junit.Assert;
import org.junit.Test;

public class OwnHashMapTest {

  @Test
  public void add_uniqueKey() {
    // Create an empty map which will contains String elements
    OwnHashMap<String> stringOwnHashMap = new OwnHashMap<>();

    // Create an object of OwnElement for String element
    OwnElement<String> stringElement = new OwnElement<>(1, "Hello world");

    // Perform the add method of the map
    int key = stringOwnHashMap.add(stringElement);

    // Since the map is empty, the key of our stringElement must be unique,
    // hence we expect that the add method will return the key of the stringElement
    Assert.assertEquals(key, stringElement.getKey());
  }

  @Test
  public void add_nonUniqueKey() {
    // Create an empty map which will contains String elements
    OwnHashMap<String> stringOwnHashMap = new OwnHashMap<>();

    // Create an object of OwnElement for String element
    OwnElement<String> stringElement = new OwnElement<>(1, "Hello world");

    // Perform the add method of the map
    stringOwnHashMap.add(stringElement);

    // Perform add again for the newElement
    int keySecondAdd = stringOwnHashMap.add(stringElement);

    // Expect that the add method will return -1 because the map already contains that element
    Assert.assertEquals(-1, keySecondAdd);
  }

  @Test
  public void remove_null() {
    OwnHashMap<String> map = new OwnHashMap<>();
    map.add(new OwnElement<>(1, "Hello world"));

    // We add key 1 but try to remove key 0, so the return value of this method must be null
    String removed = map.remove(0);
    Assert.assertNull(removed);
  }

  @Test
  public void remove_outOfRangeOrNonExistedKey() {
    OwnHashMap<String> map = new OwnHashMap<>();
    // Add key 1
    map.add(new OwnElement<>(1, "Hello world"));

    int[] nonExistedKeys = {0, -1, 2, 3, 4, Integer.MAX_VALUE, Integer.MIN_VALUE}; // any int but 1
    for (int nonExistedKey : nonExistedKeys) {
      String removed = map.remove(nonExistedKey);
      Assert.assertNull(removed);
    }
  }

  @Test
  public void remove_found() {
    OwnHashMap<String> map = new OwnHashMap<>();
    String helloWorld = "Hello world";
    OwnElement<String> newElement = new OwnElement<>(1, helloWorld);
    map.add(newElement);

    // Return value must be the same value of newElement object, since we are trying to remove its key
    // after insert it into the map
    String removed = map.remove(newElement.getKey());
    Assert.assertEquals(helloWorld, removed);
  }

  @Test
  public void size() {
    Random rand = new Random(System.currentTimeMillis());
    // Generate a random value of the expected size
    int expectedSize = rand.nextInt(100);
    OwnHashMap<Integer> map = genIntegerOwnHashMap(expectedSize);

    // After the loop, we expect that the map must contains a as much value as we have generated
    Assert.assertEquals(expectedSize, map.size());
  }

  /**
   * Utility method to generate a random int map with the given size.
   */
  private OwnHashMap<Integer> genIntegerOwnHashMap(int expectedSize) {
    Random rand = new Random(System.currentTimeMillis());
    OwnHashMap<Integer> map = new OwnHashMap<>();

    // Then, generate random values for the element based on the expected size
    for (int i = 0; i < expectedSize; i++) {
      map.add(new OwnElement<>(i, rand.nextInt()));
    }
    return map;
  }

  @Test
  public void isEmpty_true() {
    // Create an empty map
    OwnHashMap<String> emptyMap = new OwnHashMap<>();
    Assert.assertTrue(emptyMap.isEmpty());
  }

  @Test
  public void isEmpty_false() {
    // Create an map
    OwnHashMap<Integer> map = new OwnHashMap<>();
    // make sure that the map is not empty
    map.add(new OwnElement<>(1, 1));

    Assert.assertFalse(map.isEmpty());
  }

  @Test
  public void isEmpty_true_2() {
    // Create an map
    OwnHashMap<Integer> map = new OwnHashMap<>();
    // make sure that the map is not empty
    OwnElement<Integer> newElement = new OwnElement<>(1, 1);
    map.add(newElement);
    // then, remove the just added element
    map.remove(newElement.getKey());
    // Now the map must be empty again
    Assert.assertTrue(map.isEmpty());
  }

  @Test
  public void get_notFound() {
    OwnHashMap<Integer> map = genIntegerOwnHashMap(10);
    Integer integer = map.get(11);
    // Because we generate a map with only 10 element, and because we know the generation logic
    // the key can only be 0 -> (size-1). There's can never existed key 11.
    Assert.assertNull(integer);
  }

  @Test
  public void get_found() {
    OwnHashMap<Integer> map = genIntegerOwnHashMap(10);

    // Create and insert element with key 11
    OwnElement<Integer> newElement = new OwnElement<>(11, 110);
    map.add(newElement);

    Assert.assertEquals(newElement.getValue(), map.get(newElement.getKey()));
  }

  @Test
  public void print() {
    // We can't actually test the text printed to system out (too complicate), so we just use this
    // to try the print method and see the output.
    genIntegerOwnHashMap(30).print();
  }

  @Test
  public void getBucketSize_default() {
    OwnHashMap<String> map = new OwnHashMap<>();
    Assert.assertEquals(OwnHashMap.DEFAULT_SIZE, map.getBucketSize());
  }

  @Test
  public void getBucketSize_customSize() {
    int expectedBucketSize = 100;
    OwnHashMap<String> map = new OwnHashMap<>(expectedBucketSize);
    Assert.assertEquals(expectedBucketSize, map.getBucketSize());
  }

  @Test
  public void firstPartKey_positiveKey() {
    Random random = new Random(System.currentTimeMillis());
    int key = random.nextInt(1000); // to make sure it is positive
    int bucketSize = random.nextInt(100);

    OwnHashMap<String> map = new OwnHashMap<>(bucketSize);
    int firstPart = map.firstPartKey(key);

    Assert.assertEquals(key % map.getBucketSize(), firstPart);
  }

  @Test
  public void firstPartKey_negativeKey() {
    Random random = new Random(System.currentTimeMillis());
    int key = -1 - random.nextInt(1000); // to make sure it is negative
    int bucketSize = random.nextInt(100);

    OwnHashMap<String> map = new OwnHashMap<>(bucketSize);
    int firstPart = map.firstPartKey(key);

    Assert.assertEquals(Math.abs(key) % map.getBucketSize(), firstPart);
  }

  @Test
  public void secondPartKey() {
    Random random = new Random(System.currentTimeMillis());
    int key = random.nextInt();
    OwnHashMap<String> map = new OwnHashMap<>();
    int secondPart = map.secondPartKey(key);
    Assert.assertEquals(key, secondPart);
  }
}

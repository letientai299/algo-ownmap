package algo;

import java.util.Random;

public class OwnMapSortTest extends AbstractOwnSortTest {
  @Override MapInterface<Integer> getRandomValueMap(int n) {
    MapInterface<Integer> map = new OwnMap<>();
    Random random = new Random(System.currentTimeMillis());
    for (int i = 0; i < n; i++) {
      map.add(new OwnElement<>(i, random.nextInt()));
    }
    return map;
  }
}

package algo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomIntWrapperComparatorTest {

  @Test
  public void testCompare_equals() throws Exception {
    CustomIntWrapper customIntWrapper = new CustomIntWrapper(1);
    CustomIntWrapperComparator comparator = new CustomIntWrapperComparator();
    int result = comparator.compare(customIntWrapper, customIntWrapper);
    assertEquals(result, 0);
  }

  @Test
  public void testCompare_lessThan() throws Exception {
    CustomIntWrapper a = new CustomIntWrapper(1);
    CustomIntWrapper b = new CustomIntWrapper(2);
    CustomIntWrapperComparator comparator = new CustomIntWrapperComparator();
    int result = comparator.compare(a, b);
    assertTrue(result < 0);
  }

  @Test
  public void testCompare_greaterThan() throws Exception {
    CustomIntWrapper a = new CustomIntWrapper(2);
    CustomIntWrapper b = new CustomIntWrapper(1);
    CustomIntWrapperComparator comparator = new CustomIntWrapperComparator();
    int result = comparator.compare(a, b);
    assertTrue(result > 0);
  }

  @Test(expected = NullPointerException.class)
  public void testCompare_null() throws Exception {
    CustomIntWrapper a = new CustomIntWrapper(2);
    CustomIntWrapperComparator comparator = new CustomIntWrapperComparator();
    comparator.compare(a, null);
  }
}

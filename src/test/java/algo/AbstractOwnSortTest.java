package algo;

import java.util.Comparator;
import java.util.Random;
import java.util.Vector;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class AbstractOwnSortTest {

  abstract MapInterface<Integer> getRandomValueMap(int n);

  private OwnSort sorter;
  private Comparator<OwnElement<Integer>> comparator;

  @Before
  public void setup() {
    sorter = new OwnSort();
    comparator = Comparator.comparingInt(OwnElement::getValue);
  }

  @Test
  public void test_empty() throws Exception {
    MapInterface<Integer> randomValueMap = getRandomValueMap(0);
    Vector<OwnElement<Integer>> sorted = sorter.sort(randomValueMap, comparator);
    // We are testing 2 things, first, there's no exception for edge case like empty map
    // and the return container is also empty.
    assertTrue(sorted.isEmpty());
  }

  @Test
  public void test_single() throws Exception {
    MapInterface<Integer> randomValueMap = getRandomValueMap(1);
    Vector<OwnElement<Integer>> sorted = sorter.sort(randomValueMap, comparator);
    assertEquals(sorted.size(), 1);
  }

  @Test
  public void test_two() throws Exception {
    MapInterface<Integer> randomValueMap = getRandomValueMap(2);
    Vector<OwnElement<Integer>> sorted = sorter.sort(randomValueMap, comparator);
    OwnElement<Integer> a = sorted.get(0);
    OwnElement<Integer> b = sorted.get(1);
    assertTrue(comparator.compare(a, b) < 0);
  }

  @Test
  public void test_randomMultipleValue() throws Exception {
    Random random = new Random(System.currentTimeMillis());
    MapInterface<Integer> randomValueMap = getRandomValueMap(random.nextInt(100));
    Vector<OwnElement<Integer>> sorted = sorter.sort(randomValueMap, comparator);

    for (int i = 0; i < sorted.size() - 1; i++) {
      OwnElement<Integer> a = sorted.get(i);
      OwnElement<Integer> b = sorted.get(i + 1);
      assertTrue(
          String.format("Elements %d and %d should be in correct order", i, i + 1),
          comparator.compare(a, b) < 0);
    }
  }
}

package algo;

import java.util.Random;
import org.junit.Assert;
import org.junit.Test;

public class OwnMapTest {

  @Test
  public void add_uniqueKey() {
    // Create an empty map which will contains String elements
    OwnMap<String> stringOwnMap = new OwnMap<>();

    // Create an object of OwnElement for String element
    OwnElement<String> stringElement = new OwnElement<>(1, "Hello world");

    // Perform the add method of the map
    int key = stringOwnMap.add(stringElement);

    // Since the map is empty, the key of our stringElement must be unique,
    // hence we expect that the add method will return the key of the stringElement
    Assert.assertEquals(key, stringElement.getKey());
  }

  @Test
  public void add_nonUniqueKey() {
    // Create an empty map which will contains String elements
    OwnMap<String> stringOwnMap = new OwnMap<>();

    // Create an object of OwnElement for String element
    OwnElement<String> stringElement = new OwnElement<>(1, "Hello world");

    // Perform the add method of the map
    stringOwnMap.add(stringElement);

    // Perform add again for the newElement
    int keySecondAdd = stringOwnMap.add(stringElement);

    // Expect that the add method will return -1 because the map already contains that element
    Assert.assertEquals(-1, keySecondAdd);

  }

  @Test
  public void remove_null() {
    OwnMap<String> map = new OwnMap<>();
    map.add(new OwnElement<>(1, "Hello world"));

    // We add key 1 but try to remove key 0, so the return value of this method must be null
    String removed = map.remove(0);
    Assert.assertNull(removed);
  }

  @Test
  public void remove_outOfRangeOrNonExistedKey() {
    OwnMap<String> map = new OwnMap<>();
    // Add key 1
    map.add(new OwnElement<>(1, "Hello world"));

    int[] nonExistedKeys = {0, -1, 2, 3, 4, Integer.MAX_VALUE, Integer.MIN_VALUE}; // any int but 1
    for (int nonExistedKey : nonExistedKeys) {
      String removed = map.remove(nonExistedKey);
      Assert.assertNull(removed);
    }
  }

  @Test
  public void remove_found() {
    OwnMap<String> map = new OwnMap<>();
    String helloWorld = "Hello world";
    OwnElement<String> newElement = new OwnElement<>(1, helloWorld);
    map.add(newElement);

    // Return value must be the same value of newElement object, since we are trying to remove its key
    // after insert it into the map
    String removed = map.remove(newElement.getKey());
    Assert.assertEquals(helloWorld, removed);
  }

  @Test
  public void size() {
    Random rand = new Random(System.currentTimeMillis());
    // Generate a random value of the expected size
    int expectedSize = rand.nextInt(100);
    OwnMap<Integer> map = genIntegerOwnMap(expectedSize);

    // After the loop, we expect that the map must contains a as much value as we have generated
    Assert.assertEquals(expectedSize, map.size());
  }

  /**
   * Utility method to generate a random int map with the given size.
   */
  private OwnMap<Integer> genIntegerOwnMap(int expectedSize) {
    Random rand = new Random(System.currentTimeMillis());
    OwnMap<Integer> map = new OwnMap<>();

    // Then, generate random values for the element based on the expected size
    for (int i = 0; i < expectedSize; i++) {
      map.add(new OwnElement<>(i, rand.nextInt()));
    }
    return map;
  }

  @Test
  public void isEmpty_true() {
    // Create an empty map
    OwnMap<String> emptyMap = new OwnMap<>();
    Assert.assertTrue(emptyMap.isEmpty());
  }

  @Test
  public void isEmpty_false() {
    // Create an map
    OwnMap<Integer> map = new OwnMap<>();
    // make sure that the map is not empty
    map.add(new OwnElement<>(1, 1));

    Assert.assertFalse(map.isEmpty());
  }

  @Test
  public void isEmpty_true_2() {
    // Create an map
    OwnMap<Integer> map = new OwnMap<>();
    // make sure that the map is not empty
    OwnElement<Integer> newElement = new OwnElement<>(1, 1);
    map.add(newElement);
    // then, remove the just added element
    map.remove(newElement.getKey());
    // Now the map must be empty again
    Assert.assertTrue(map.isEmpty());
  }

  @Test
  public void get_notFound() {
    OwnMap<Integer> map = genIntegerOwnMap(10);
    Integer integer = map.get(11);
    // Because we generate a map with only 10 element, and because we know the generation logic
    // the key can only be 0 -> (size-1). There's can never existed key 11.
    Assert.assertNull(integer);
  }

  @Test
  public void get_found() {
    OwnMap<Integer> map = genIntegerOwnMap(10);

    // Create and insert element with key 11
    OwnElement<Integer> newElement = new OwnElement<>(11, 110);
    map.add(newElement);

    Assert.assertEquals(newElement.getValue(), map.get(newElement.getKey()));
  }

  @Test
  public void print() {
    // We can't actually test the text printed to system out (too complicate), so we just use this
    // to try the print method and see the output.
    genIntegerOwnMap(10).print();
  }
}

package algo;

import java.util.Random;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

public abstract class AbstractMapInterfaceIteratorTest {

  abstract MapInterface<Integer> getRandomValueMap(int n);

  private MapInterface<Integer> getEmptyMap() {
    return getRandomValueMap(0);
  }

  private MapInterface<Integer> getSingleItemMap() {
    return getRandomValueMap(1);
  }

  @Test
  public void test_hasNext_emptyMap() throws Exception {
    MapInterface<Integer> map = getEmptyMap();
    AbstractIterator<OwnElement<Integer>> iterator = map.iterator();
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void test_hasNext_singleItem() throws Exception {
    MapInterface<Integer> map = getSingleItemMap();
    AbstractIterator<OwnElement<Integer>> iterator = map.iterator();
    // we expect to have a single item, so the first call to next() should be ok.
    iterator.next();

    // And after that, there's no more item, hence, hasNext() should return false
    Assert.assertFalse(iterator.hasNext());
  }

  @Test
  public void test_hasNext_multi() throws Exception {
    int n = 10;
    MapInterface<Integer> randomValueMap = getRandomValueMap(10);
    AbstractIterator<OwnElement<Integer>> iterator = randomValueMap.iterator();
    int counter = 0;
    while (iterator.hasNext()) {
      counter++;
      iterator.next();
    }

    assertEquals(counter, n);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void test_remove_emptyMap() {
    MapInterface<Integer> singleItemMap = getEmptyMap();
    AbstractIterator<OwnElement<Integer>> iterator = singleItemMap.iterator();
    iterator.remove();
  }

  @Test(expected = UnsupportedOperationException.class)
  public void test_remove_multi() {
    MapInterface<Integer> singleItemMap = getRandomValueMap(10);
    AbstractIterator<OwnElement<Integer>> iterator = singleItemMap.iterator();
    iterator.remove();
  }

  @Test
  public void test_get_single() throws Exception {
    MapInterface<Integer> map = getSingleItemMap();
    AbstractIterator<OwnElement<Integer>> iterator = map.iterator();
    OwnElement<Integer> result = iterator.get();
    assertNotNull(result);

    // calling get again should yield same result, same object reference,
    // because we expect get should increase the iterator
    OwnElement<Integer> resultAgain = iterator.get();
    assertSame(result, resultAgain);
  }

  @Test
  public void test_get_multi() throws Exception {
    MapInterface<Integer> map = getRandomValueMap(15);
    AbstractIterator<OwnElement<Integer>> iterator = map.iterator();
    OwnElement<Integer> result = iterator.get();
    assertNotNull(result);
    OwnElement<Integer> resultAgain = iterator.get();
    assertSame(result, resultAgain);
  }

  @Test
  public void test_reset_multi() throws Exception {
    int n = 10;
    MapInterface<Integer> map = getRandomValueMap(n);
    AbstractIterator<OwnElement<Integer>> iterator = map.iterator();
    Random random = new Random();
    int loopFirstRound = random.nextInt(n);

    while (loopFirstRound-- > 0) {
      iterator.next();
    }

    iterator.reset();

    int counter = 0;

    while (iterator.hasNext()) {
      iterator.next();
      counter++;
    }

    assertEquals(n, counter);
  }

  @Test
  public void test_iterator() throws Exception {
    MapInterface<Integer> map = getRandomValueMap(5);
    AbstractIterator<OwnElement<Integer>> iterator = map.iterator();
    assertSame(iterator, iterator.iterator());
  }
}

package algo;

/**
 * The data structure that contains elements consist of key and value (object). The stored elements
 * are of type {@link OwnElement} and the key (in {@link OwnElement}) is used to address the value.
 */
public interface MapInterface<T> {

  /**
   * If the key in the newElement object is not existed in the map, store the newElement and return
   * its key. Otherwise, return -1, and do no modification with the map.
   */
  int add(OwnElement<T> newElement);

  /**
   * Iterates through the map and removes the element that has same key with the input.
   */
  T remove(int key);

  /**
   * Returns the number of {@link OwnElement} in the map.
   */
  int size();

  /**
   * Returns true if the map is empty, false otherwise.
   */
  boolean isEmpty();

  /**
   * Iterates through the map and return the {@link OwnElement} with the key value.
   *
   * Return null if not found.
   */
  T get(int key);

  /**
   * Prints the {@link OwnElement}s in the order of how they are stored in the map.
   */
  void print();

  AbstractIterator<OwnElement<T>> iterator();
}

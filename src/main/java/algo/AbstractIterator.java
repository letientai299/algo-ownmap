package algo;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Abstract Iterator from slide.
 */
public abstract class AbstractIterator<E> implements Enumeration<E>, Iterator<E>, Iterable<E> {
  /**
   * Reset the iterator to the beginning of the structure.
   */
  public abstract void reset();

  // hasNext is skipped since we already have it defined in the Iterator interface.

  /**
   * Return current value but don't increment the iterator.
   */
  public abstract E get();

  // next is skipped since it's is defined in the Iterator interface

  /**
   * Modified from the slide, use standard java exception instead of junit library.
   */
  @Override public void remove() {
    throw new UnsupportedOperationException("Remove are not implemented");
  }

  @Override public boolean hasMoreElements() {
    return hasNext();
  }

  /**
   * Return the current value and increment the iterator
   */
  @Override public E nextElement() {
    return next();
  }

  /**
   * Return itself as a subject for a for-loop
   */
  @Override public Iterator<E> iterator() {
    return this;
  }
}

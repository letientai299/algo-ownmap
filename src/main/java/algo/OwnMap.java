package algo;

import java.util.Iterator;
import java.util.Vector;

public class OwnMap<T> implements MapInterface<T> {
  private final Vector<OwnElement<T>> bucketList = new Vector<>();

  /**
   * Makes an empty map in which initial elements can be stored.
   */
  public OwnMap() {
    // Explicit define the method to match with the requirements.
  }

  @Override public int add(OwnElement<T> newElement) {
    for (OwnElement<T> ownElement : bucketList) {
      if (ownElement.getKey() == newElement.getKey()) {
        return -1;
      }
    }
    bucketList.add(newElement);
    return newElement.getKey();
  }

  @Override public T remove(int key) {
    Iterator<OwnElement<T>> iterator = bucketList.iterator();

    // Use iterator loop style to avoid concurrent modification exception
    while (iterator.hasNext()) {
      OwnElement<T> next = iterator.next();

      if (next.getKey() == key) {
        // Remove the element from the collection
        iterator.remove();

        // return the removed element's value
        return next.getValue();
      }
    }

    return null;
  }

  @Override public int size() {
    return bucketList.size();
  }

  @Override public boolean isEmpty() {
    return bucketList.isEmpty();
  }

  @Override public T get(int key) {
    for (OwnElement<T> ownElement : bucketList) {
      if (ownElement.getKey() == key) {
        return ownElement.getValue();
      }
    }
    return null;
  }

  @Override public void print() {
    System.out.println(this.toString());
  }

  @Override public AbstractIterator<OwnElement<T>> iterator() {
    return new OwnMapIterator<>(this);
  }

  @Override public String toString() {
    StringBuilder sb = new StringBuilder("OwnMap{" + System.lineSeparator());
    for (OwnElement<T> ownElement : bucketList) {
      sb.append("  ")
          .append(ownElement.toString())
          .append(",")
          .append(System.lineSeparator());
    }
    sb.append("}");
    return sb.toString();
  }

  private static class OwnMapIterator<T> extends AbstractIterator<OwnElement<T>> {

    private final Vector<OwnElement<T>> container;
    private int index = 0;

    OwnMapIterator(OwnMap<T> ownMap) {
      super();
      this.container = ownMap.bucketList;
    }

    @Override public void reset() {
      index = 0;
    }

    @Override public OwnElement<T> get() {
      return container.get(index);
    }

    @Override public boolean hasNext() {
      return index < container.size();
    }

    @Override public OwnElement<T> next() {
      OwnElement<T> element = container.get(index);
      index++;
      return element;
    }
  }
}

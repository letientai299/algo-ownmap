package algo;

/**
 * Contains key and value that will be stored within the custom map.
 */
public class OwnElement<T> { // Use generic to support many types in runtime.
  /**
   * The key for this element.
   */
  // Key is not modifiable since there's no setter required in the assignment. Hence, we define them
  // as final
  private final int key;

  private T value;

  /**
   * Create an object with only the key provided. Value by default is null.
   */
  public OwnElement(int key) {
    this.key = key;
  }

  /**
   * Create an object with both key and value provided.
   */
  public OwnElement(int key, T value) {
    this.key = key;
    this.value = value;
  }

  /**
   * Return the key.
   */
  public int getKey() {
    return key;
  }

  /**
   * Return the value.
   */
  public T getValue() {
    return value;
  }

  /**
   * Set value and return the passed value.
   */
  public T setValue(T value) {
    this.value = value;
    return this.value;
  }

  @Override public String toString() {
    return "OwnElement{" +
        "key=" + key +
        ", value=" + value +
        '}';
  }
}

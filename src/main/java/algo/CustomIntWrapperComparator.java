package algo;

import java.util.Comparator;

public class CustomIntWrapperComparator implements Comparator<CustomIntWrapper> {

  @Override public int compare(CustomIntWrapper a, CustomIntWrapper b) {
    return Integer.compare(a.getValue(), b.getValue());
  }
}

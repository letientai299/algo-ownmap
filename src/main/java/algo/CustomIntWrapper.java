package algo;

/**
 * A simple int wrapper.
 */
public class CustomIntWrapper {
  private final int value;

  public CustomIntWrapper(int value) {
    this.value = value;
  }

  public int getValue() {
    return this.value;
  }
}

package algo;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

public class OwnSort {
  public <T> Vector<OwnElement<T>> sort(MapInterface<T> map, Comparator<OwnElement<T>> comparator) {
    // Call the concrete implementation.
    // We choose a general name for the public method, so that if we want to change to other algorithm
    // later, we don't need to change the public API.
    return selectionSort(map, comparator);
  }

  private <T> Vector<OwnElement<T>> selectionSort(MapInterface<T> map, Comparator<OwnElement<T>> comparator) {
    Iterator<OwnElement<T>> iterator = map.iterator();

    // To store the elements after sorted.
    Vector<OwnElement<T>> sortedVector = new Vector<>();
    while (iterator.hasNext()) {
      sortedVector.add(iterator.next());
    }

    for (int i = 0; i < sortedVector.size() - 1; i++) {
      OwnElement<T> min = sortedVector.get(i);
      int minIndex = i;
      for (int j = i + 1; j < sortedVector.size(); j++) {
        OwnElement<T> a = sortedVector.get(j);
        // compare the current min value with the next value in the map,
        if (comparator.compare(min, a) > 0) {
          min = a;
          minIndex = j;
        }
      }

      // Save the key-value pair into the sorted container.
      OwnElement<T> backup = sortedVector.get(i);
      sortedVector.set(i, min);
      sortedVector.set(minIndex, backup);
    }

    return sortedVector;
  }
}


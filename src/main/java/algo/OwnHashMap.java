package algo;

public class OwnHashMap<T> implements MapInterface<T> {
  static final int DEFAULT_SIZE = 10;
  private final int bucketSize;
  private final OwnMap<OwnMap<T>> bucketMap;

  public OwnHashMap() {
    this(DEFAULT_SIZE);
  }

  public OwnHashMap(int size) {
    this.bucketSize = size;
    this.bucketMap = new OwnMap<>();

    // prepare the sub map that later will be used to store the concrete elements
    for (int i = 0; i < size; i++) {
      bucketMap.add(new OwnElement<>(i, new OwnMap<>()));
    }
  }

  public int getBucketSize() {
    return bucketSize;
  }

  @Override public int add(OwnElement<T> newElement) {
    int key = newElement.getKey();
    OwnMap<T> ownMap = getSubOwnMap(key);
    return ownMap.add(newElement);
  }

  @Override public T remove(int key) {
    OwnMap<T> ownMap = getSubOwnMap(key);
    return ownMap.remove(key);
  }

  private OwnMap<T> getSubOwnMap(int key) {
    OwnMap<T> ownMap = bucketMap.get(firstPartKey(key));
    if (ownMap == null) {
      throw new IllegalStateException(
          "The sub map should never be null. There must be a bug in firstPartKey method.");
    }
    return ownMap;
  }

  @Override public int size() {
    int totalSize = 0;
    for (int i = 0; i < bucketSize; i++) {
      totalSize += bucketMap.get(i).size();
    }
    return totalSize;
  }

  @Override public boolean isEmpty() {
    for (int i = 0; i < bucketSize; i++) {
      // If one of the sub map is not empty, then this hash map is not empty
      if (!bucketMap.get(i).isEmpty()) {
        return false;
      }
    }

    // if we reach this point, then all of the sub map are empty, hence, this is empty
    return true;
  }

  @Override public T get(int key) {
    OwnMap<T> ownMap = getSubOwnMap(key);
    return ownMap.get(key);
  }

  @Override public void print() {
    System.out.println(this.toString());
  }

  @Override public AbstractIterator<OwnElement<T>> iterator() {
    return new OwnHashMapIterator<>(this);
  }

  @Override public String toString() {
    StringBuilder sb = new StringBuilder("OwnHashMap{").append(System.lineSeparator());
    for (int i = 0; i < bucketSize; i++) {
      OwnMap<T> ownMap = bucketMap.get(i);
      String[] strings = ownMap.toString().split(System.lineSeparator());

      for (String s : strings) {
        sb.append("  ").append(s).append(System.lineSeparator());
      }

      sb.append(",").append(System.lineSeparator());
    }
    sb.append("}");
    return sb.toString();
  }

  /**
   * Return the key fo rth bucket in which the {@link OwnMap} with the key value is stored. In our
   * case this key is key modulo size.
   */
  protected int firstPartKey(int key) {
    long keyLong = key < 0 ? 0L - key : key;
    return (int) (keyLong % this.bucketSize);
  }

  /**
   * Return the second part of the key, with which the {@link OwnElement} in the selected bucket is
   * found. In this case, the given keyA itself.
   */
  protected int secondPartKey(int key) {
    return key;
  }

  public static class OwnHashMapIterator<T> extends AbstractIterator<OwnElement<T>> {

    private final AbstractIterator<OwnElement<OwnMap<T>>> mapIterator;
    private final OwnHashMap<T> map;
    private AbstractIterator<OwnElement<T>> currentSubIterator;
    private int counter = 0;

    OwnHashMapIterator(OwnHashMap<T> map) {
      super();
      this.map = map;
      this.mapIterator = this.map.bucketMap.iterator();
      resetMapIterator();
    }

    @Override public void reset() {
      resetMapIterator();
    }

    private void resetMapIterator() {
      counter = 0;
      mapIterator.reset();
      OwnElement<OwnMap<T>> element = this.mapIterator.next();
      OwnMap<T> currentMap = element.getValue();
      currentSubIterator = currentMap.iterator();
    }

    @Override public OwnElement<T> get() {
      return currentSubIterator.get();
    }

    private void increaseSubIterator() {
      currentSubIterator = mapIterator.next().getValue().iterator();
    }

    @Override public boolean hasNext() {
      return counter < map.size();
    }

    @Override public OwnElement<T> next() {
      if (!currentSubIterator.hasNext()) {
        increaseSubIterator();
      }
      counter++;
      return currentSubIterator.next();
    }
  }
}

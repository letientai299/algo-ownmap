OwnMap Implementation
=====================

Result for the assignment described in ExcersiseDSC1versie3.pdf

Build
-----

- To build using maven in console, run:

```
./mvnw build
```


- To build using IDE, just import this project into any IDE as a maven project, the IDE will pick it 
up perfectly.

Test
----

Run test in the console

```
./mvnw test
```


DS2017-Excercise2
-----------------

### Comparator

- See the `CustomIntWrapper` for the own data type that we have defined. It contains a single int value.
- The `CustomIntWrapperComparator` implement `Comparator` interface to compare 2 objects of type 
`CustomIntWrapper`. Because we know that underlying the custom type, there's only a single `int`, 
we can make use of `Integer.compare()` method to do the comparision.
- `OwnSort` utilizes the `iterator` method, which is defined for the next exercise, to make the code 
more simpler and also share algorithm between different implementation for `MapInterface`. So, `OwnSort` 
can work with any other implementation, not tight to `OwnMap` and `OwnHashMap`.

### Iterator

We add one more method into `MapInterface`: `iterator` which will return an `AbstractIterator` that 
allow us to loop through all the underlying elements within the `MapInterface.` The abstract class 
is copied with slightly modification from the slide and is extent as private static classes in 
`OwnMap` and `OwnHashMap`. For the implementation in `OwnHashMap`, we have tried hard to reuse as 
much as possible methods of the iterator from `OwnMap`.

`OwnHashMap` and `OwnMap` implement same interface, hence the test for that API should be applicable 
for both of them. We have show that by only write test in the `AbstractMapInterfaceIteratorTest` class,
and provide the concrete implementation as the object under test in its 2 subclasses.


